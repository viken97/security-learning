## Spring Security 实战教程配套代码 

最新、最全、最干，觉得不错请  ⭐ **Star**一下 ⭐ ，如果觉得不错一定要安利给其他同学。

>  📢胖哥又一力作， **Spring Security OAuth2** 相关教程已经正式发布🚀， **Spring Security** 最新版本，结合 **Spring** 家族新成员 **Spring Authorization Server**  和 **Spring Cloud**  附带DEMO。详细讲解参见[（点击学习 :arrow_right: ）Spring Security 与 OAuth2](https://blog.csdn.net/qq_35067322/category_11691173.html)专题教程

个人博客  [码农小胖哥的博客:https://www.felord.cn/](https://www.felord.cn/)

### 另有Spring Security开源登录插件，简单好用，无侵入

[spring-security-login-extension](https://gitee.com/felord/spring-security-login-extension)，觉得不错请  ⭐ **Star**一下 ⭐


请多多关注公众号：码农小胖哥

遇到问题，不清楚的地方可以及时沟通
 
 ![](./qr.jpg)
 
 